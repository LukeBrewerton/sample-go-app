# Sample Go App

[![pipeline status](https://gitlab.com/LukeBrewerton/sample-go-app/badges/master/pipeline.svg)](https://gitlab.com/LukeBrewerton/sample-go-app/commits/master)

This is a simple Hello World app written in Go. It also has a docker file to build the container and also uses GitLab CI for the build and deploy.